import React from 'react';

class VideoDetail extends React.Component{
    state = {video:this.props.video};

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({video:nextProps.video})
    }


    render(){
        let video = this.state.video;
        if(!video){
            return <div className={"ui active centered inline loader"} />

        }
        return (
            <div>
                <div className={'ui embed'}>
                    <iframe title={video.id.videoId} src={`http://www.youtube.com/embed/${video.id.videoId}`}/>
                </div>
                <div className={'ui segment'}>
                    <h4 className={'ui header'}> {video.snippet.title} </h4>
                    <p>
                        {video.snippet.description}
                    </p>
                </div>
            </div>)
    }
}

export default VideoDetail;