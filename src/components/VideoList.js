import React from 'react';
import VideoItem from './VideoItem';

class VideoList extends React.Component {
    createVideoItem = (video) => {
        return (
            <VideoItem
                key={video.id.videoId}
                selectedVideo={this.props.selectedVideo}
                video={video}/>
        )
    };

    renderVideoItems = (videos) => {
        return (
            videos.map((video) => this.createVideoItem(video))
        )
    };

    render() {
        return (
            <div className={'ui relaxed divided list'}>
                {this.renderVideoItems(this.props.videos)}
            </div>
        );
    }
}

export default VideoList;