import React from 'react';
import SearchBar from "./SearchBar";
import VideoList from "./VideoList";
import VideoDetail from "./VideoDetail";
import youtube from '../apis/youtube';

class App extends React.Component {
    state = {videos: [], selectedVideo: null};

    componentDidMount() {
        this.onTermSubmit('react')
    }

    onVideoSelect = (video) => {
        this.setState({selectedVideo: video}, () => {
            console.log(this.state.selectedVideo)
        });
    };

    onTermSubmit = async (term) => {
        const response = await youtube.get('/search', {
            params: {
                q: term
            }
        });
        this.setState({videos: response.data.items, selectedVideo: null});
    };

    render() {

        return (
            <div className={"ui container"} style={{"margin": "10px"}}>
                <div className={'ui segment'}>
                    <SearchBar onFormSubmit={this.onTermSubmit}/>
                </div>
                <div className={'ui grid'}>
                    <div className={'ui row'}>
                        <div className={'eleven wide column'}>
                            <VideoDetail video={this.state.selectedVideo || this.state.videos[0]}/>
                        </div>
                        <div className={'five wide column'}>
                            <VideoList selectedVideo={this.onVideoSelect} videos={this.state.videos}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default App;
