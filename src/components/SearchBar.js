import React, {Component} from "react";

class SearchBar extends Component {
    state = {term: ""};

    onSubmitHandler = e => {
        e.preventDefault();
        this.props.onFormSubmit(this.state.term);
        this.setState({term: ""});
    };

    render() {
        return (
            <form
                className={"ui form"}
                onSubmit={this.onSubmitHandler}
            >
                <div className={"ui fluid icon input"}>
                    <i className={"search icon"}/>
                    <input
                        value={this.state.term}
                        onChange={e => {
                            this.setState({term: e.target.value});
                        }}
                        placeholder={"Term to search for"}
                    />
                </div>
            </form>
        );
    }
}

export default SearchBar;
