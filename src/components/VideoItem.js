import React, {Component} from 'react';
import './VideoItem.css';
class VideoItem extends Component {
    state = {video : this.props.video};
    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({video : nextProps.video})
    }

    onClickVideo = () => {
        this.props.selectedVideo(this.state.video)
    };

    render() {
        return (
            <div className={'video-item item'}
                 onClick={this.onClickVideo}>
                <img className={'ui image'}
                     alt={this.state.video.snippet.title}
                     src={this.state.video.snippet.thumbnails.medium.url} />
                <div className={'content'}>
                    <div className={'header'}>
                        {this.state.video.snippet.title}
                    </div>
                </div>
            </div>
        );
    }
}

export default VideoItem;